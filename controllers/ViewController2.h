//
//  ViewController2.h
//  controllers
//
//  Created by Prince Prabhakar on 06/11/15.
//  Copyright (c) 2015 Prince Prabhakar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController2 : UIViewController
@property (strong, nonatomic) IBOutlet UIView *label2;
@property (strong, nonatomic) IBOutlet UIView *button2;

@end
