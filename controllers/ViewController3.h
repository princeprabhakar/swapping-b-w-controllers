//
//  ViewController3.h
//  controllers
//
//  Created by Prince Prabhakar on 06/11/15.
//  Copyright (c) 2015 Prince Prabhakar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController3 : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *label3;
@property (strong, nonatomic) IBOutlet UIButton *button3;

@end
