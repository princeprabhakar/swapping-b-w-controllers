//
//  ViewController5.h
//  controllers
//
//  Created by Prince Prabhakar on 06/11/15.
//  Copyright (c) 2015 Prince Prabhakar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController5 : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *label5;
@property (strong, nonatomic) IBOutlet UIButton *button5;

@end
